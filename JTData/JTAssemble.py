import re
from JTDecode.JTFileReader import JTFileReader
from JTData.JTObjectFinder import JTObjectFinder

LITTLE_ENDIAN = 0x00
IS_COMPRESSED = 0x02

PARTITION_NODE = (0x10dd103e, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97)


class JTAssemble:
	_jtFileReader = None

	jtVersion = 0.0
	_tocOffset = 0

	_lSGSegID = None

	def __init__(self, filePath):
		assembleFile = open(filePath, 'rb')
		self._jtFileReader = JTFileReader(assembleFile)


	def readHeader(self):
		versionStr, endianness = self._jtFileReader.readStruct("80sB")
		self.jtVersion = float(re.search(r'Version\s*([0-9].[0-9])', versionStr.decode("ascii")).group(1))
		if endianness == LITTLE_ENDIAN:
			self._jtFileReader.endianness = '<'
		else:
			self._jtFileReader.endianness = '>'
		reserved, self._tocOffset, self._lSGSegID = self._jtFileReader.readStruct("iiG")

	def readTOC(self):
		self._jtFileReader.seek(self._tocOffset)
		(entries, ) = self._jtFileReader.readStruct("i")

		segments = []
		for i in range(entries):
			segmentID, segmentOffset, segmentLength, segmentAttributes = self._jtFileReader.readStruct("GiiI")

			segment = Segment(segmentID, segmentOffset)
			segments.append(segment)


		self.sortSegments(segments, self._lSGSegID)

		for i in range(len(segments)):
			self.readSegment(segments[i])

		"""
		#read segments
		for segmentOffset in segmentOffsets:
			self.readSegment(segmentOffset[1])
		"""

	def sortSegments(self, segments, lsdID):
		#put LSGSegment first
		foundLSG = False
		for i in range(len(segments)):
			if segments[i].segmentID == lsdID:
				foundLSG = True
				segments[i], segments[0] = segments[0], segments[i]

		if foundLSG == False:
			raise Exception("Can't find LSG data segment in JT file")

	def readSegment(self, segment):
		# find more details about each one and group same objects together
		self._jtFileReader.seek(segment.segmentOffset)
		# header
		segmentID, segmentType, segmentLength = self._jtFileReader.readStruct("Gii")
		isPotentCompressed = False
		if segmentType in [1, 2, 3, 4, 17, 18, 20, 24]:
			# is a segment type that could be compressed (e.g. Meta data)
			isPotentCompressed = True

		# data
		if isPotentCompressed == True:
			# Logical Element Header ZLIB
			compressFlag, compressedDataLength, compressionAlgorithm = self._jtFileReader.readStruct("IiB")

			if compressFlag == IS_COMPRESSED:
				# data segment is compressed
				# compressedDataLength includes "CompressionAlgorithm" for some reason
				actualCompressLength = compressedDataLength - self._jtFileReader.calcSize("B")
				self._jtFileReader.uncompressSegment(actualCompressLength, compressionAlgorithm)

		# Logical Element Header
		ElementLength, objTypeID, ObjectBaseType, objectID = self._jtFileReader.readStruct("iGBi")

		objFinder = JTObjectFinder()
		jtObject = objFinder.findObjectType(objTypeID)
		if jtObject != None:
			jtObject.readElement(self._jtFileReader, self)

class Segment:
	segmentID = None
	segmentOffset = None

	def __init__(self, segmentID, segmentOffset):
		self.segmentID = segmentID
		self.segmentOffset = segmentOffset