from JTObjectTypes.BaseNode import BaseNode
from JTObjectTypes.PartitionNode import PartitionNode
from JTObjectTypes.TriStripSetNode import TriStripSetNode

class JTObjectFinder():
    def findObjectType(self, objGuid):
        self.printGuid(objGuid)

        if BaseNode.ObjectGUID == objGuid:
            return BaseNode()
        elif PartitionNode.ObjectGUID == objGuid:
            return PartitionNode()
        elif TriStripSetNode.ObjectGUID == objGuid:
            return TriStripSetNode()
        else:
            print("can't find object type")

    def printGuid(self, objGuid):
        # convert from base 10 to hex
        guidHexed = tuple(hex(x) for x in objGuid)
        print(guidHexed)