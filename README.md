**BlenderJT**
##Intro
This is a repo for importing .JT models into blender. The JT file format is describe here:
https://en.wikipedia.org/wiki/JT_(visualization_format)

Unfortunately this project has been abandoned for the foreseeable future (as detailed below).

##Purpose
I was given a couple of thousand 3D CAD models for a task I was assigned to at work. Each model was in a seperate .JT file and needed further cleanup before the models could be presented to a human. Blender currently didn't have any existing plugin to import .jt files so I decided to start this project. Unfortunately I later found out later that Siemens' never openly released any file format specification previous to version 9.5. The JT model I was working with where previous to version 9.5. This turned out to be a large problem as .JT files are not backwards compatible and vary significantly between versions. I have requested the documentation from Siemens' but with no luck. Until Siemen's provides me with the documentation this project will be suspended.