from JTObjectTypes.JTObject import JTObject
from JTObjectTypes.GroupNode import GroupNode

class PartitionNode(JTObject):
    ObjectGUID = (0x10dd103e, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97)

    groupNode = None
    fileName = None
    transformedBBox = None
    untransformBBox = None
    area = None
    vertexCountMin = None
    vertexCountMax = None
    nodeCountRangeMin = None
    nodeCountRangeMax = None
    polygonCountMin = None
    polygonCountMax = None

    def __init__(self):
        self.groupNode = GroupNode()

    def readElement(self, jtFileReader, jtAssemble):
        self.groupNode.readElement(jtFileReader, jtAssemble)

        (partitionFlags, self.fileName, transformBBox, self.area,
         self.vertexCountMin, self.vertexCountMax, self.nodeCountRangeMin,
         self.nodeCountRangeMax, self.polygonCountMin, self.polygonCountMax) = jtFileReader.readStruct("iMXfiiiiii")

        if partitionFlags & jtFileReader.BIT0 == True:
            self.transformedBBox = transformBBox
        else:
            (untransformBox, ) = jtFileReader.readStruct("X")
            self.untransformBBox = untransformBox

