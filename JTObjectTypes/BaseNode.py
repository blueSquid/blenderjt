from JTObjectTypes.JTObject import JTObject

class BaseNode(JTObject):
    ObjectGUID = (0x10dd1035, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97)

    BASE_NODE_VERSION = 0x01

    ignoreFlag = None
    attributeObjs = []

    def readElement(self, jtFileReader, jtAssemble):
        if jtAssemble.jtVersion > 8.0:
            (versionNum,) = jtFileReader.readStruct("h")
            if versionNum != BaseNode.BASE_NODE_VERSION:
                raise Exception("Invalid base version.")
            pass

        nodeFlags, attributeCount = jtFileReader.readStruct("Ii")

        self.ignoreFlag = nodeFlags & jtFileReader.BIT0

        # load attributes
        for i in range(attributeCount):
            (attributeObjId, ) = jtFileReader.readStruct("i")
            self.attributeObjs.append(attributeObjId)