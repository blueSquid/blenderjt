from JTObjectTypes.JTObject import JTObject
class BaseShapeLODData(JTObject):
    ObjectGUID = (0x10dd10a4, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97)

    BASE_SHAPE_LOD_VERSION = 0x01

    def readElement(self, jtFileReader, jtAssemble):
        (versionNum,) = jtFileReader.readStruct("h")

        if versionNum != self.BASE_SHAPE_LOD_VERSION:
            raise Exception("Invalid Base Shape LOD Data version.")

