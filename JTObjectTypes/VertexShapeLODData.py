from JTObjectTypes.TopoMeshCompressedRepDataV1 import TopoMeshCompressedRepDataV1
from JTObjectTypes.TopoMeshCompressedRepDataV2 import TopoMeshCompressedRepDataV2

class VertexShapeLODData():
    TOPO_MESH_VER_1 = 0x01
    TOPO_MESH_VER_2 = 0x02

    topoMeshCompressedRepDataV1 = None
    topoMeshCompressedRepDataV2 = None
    vertexRecordsObjectId = None

    def __init__(self):
        pass


    def readElement(self, jtFileReader, jtAssemble, isFromTriStrip):
        (versionNum,) = jtFileReader.readStruct("h")
        (vertexBinding,) = jtFileReader.readStruct("Q")

        if isFromTriStrip == True:
            self._readTopoMeshTopologicallyCompressedLODData(jtFileReader, jtAssemble)
        else:
            self._readTopoMeshCompressedLODData(jtFileReader, jtAssemble)


    def _readTopoMeshCompressedLODData(self, jtFileReader, jtAssemble):
        self._topoMeshLODData(jtFileReader, jtAssemble)

        # version num
        (topMeshVersionNum,) = jtFileReader.readStruct("h")

        if topMeshVersionNum == self.TOPO_MESH_VER_1:
            self.topoMeshCompressedRepDataV1 = TopoMeshCompressedRepDataV1()
            self.topoMeshCompressedRepDataV1.readElement(jtFileReader, jtAssemble)
        elif topMeshVersionNum == self.TOPO_MESH_VER_2:
            self.topoMeshCompressedRepDataV2 = TopoMeshCompressedRepDataV2()
            self.topoMeshCompressedRepDataV2.readElement(jtFileReader, jtAssemble)
        else:
            raise Exception("Invalid topo mesh version.")

    def _topoMeshLODData(self, jtFileReader, jtAssemble):
        (topMeshVersionNum,) = jtFileReader.readStruct("h")

        if topMeshVersionNum != self.TOPO_MESH_VER_1 and topMeshVersionNum != self.TOPO_MESH_VER_2:
            raise Exception("Invalid topomesh LOD Data version.")

        (self.vertexRecordsObjectId,) = jtFileReader.readStruct("i")

    def _readTopoMeshTopologicallyCompressedLODData(self, jtFileReader, jtAssemble):
        (topMeshVersionNum,) = jtFileReader.readStruct("h")
        self._readTopologicallyCompressedRepData(jtFileReader, jtAssemble)

    def _readTopologicallyCompressedRepData(self, jtFileReader, jtAssemble):
        (faceDegrees, ) = jtFileReader.readStruct("V")
        (faceDegrees,) = jtFileReader.readStruct("V")
        (faceDegrees,) = jtFileReader.readStruct("V")
        (faceDegrees,) = jtFileReader.readStruct("V")
        (faceDegrees,) = jtFileReader.readStruct("V")
        (faceDegrees,) = jtFileReader.readStruct("V")
        (faceDegrees,) = jtFileReader.readStruct("V")
        (faceDegrees,) = jtFileReader.readStruct("V")

        pass






