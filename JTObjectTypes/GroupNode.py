from JTObjectTypes.JTObject import JTObject
from JTObjectTypes.BaseNode import BaseNode

class GroupNode(JTObject):
    ObjectGUID = (0x10dd101b, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97)

    GROUP_NODE_VERSION = 0x001

    baseNode = None
    nodeChildObjectIDs = []

    def __init__(self):
        self.baseNode = BaseNode()

    def readElement(self, jtFileReader, jtAssemble):

        self.baseNode.readElement(jtFileReader, jtAssemble)

        if jtAssemble.jtVersion > 8.0:
            (versionNum,) = jtFileReader.readStruct("h")
            if versionNum != self.GROUP_NODE_VERSION:
                raise Exception("Invalid group version.")
            pass

        (childCount,) = jtFileReader.readStruct("i")
        for child in range(childCount):
            (childNodeID, ) = jtFileReader.readStruct("i")
            self.nodeChildObjectIDs.append(childNodeID)
        pass