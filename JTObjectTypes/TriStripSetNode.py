from JTObjectTypes.VertexShapeLODData import VertexShapeLODData
from JTObjectTypes.BaseShapeLODData import BaseShapeLODData
from JTObjectTypes.JTObject import JTObject
class TriStripSetNode(JTObject):
    ObjectGUID = (0x10dd10ab, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97)

    baseShapeLODData = None
    vertexShapeLODData = None

    def __init__(self):
        self.baseShapeLODData = BaseShapeLODData()
        self.vertexShapeLODData = VertexShapeLODData()


    def readElement(self, jtFileReader, jtAssemble):
        self.baseShapeLODData.readElement(jtFileReader, jtAssemble)

        isFromTriStrip = True
        self.vertexShapeLODData.readElement(jtFileReader, jtAssemble, isFromTriStrip)

        (versionNum,) = jtFileReader.readStruct("h")
        if versionNum != 0x0001:
            raise Exception("Invalid version for TriStripSetNode")
