import struct
import zlib

ZLIB_COMPRESSION = 0x02

class JTFileReader:
	BIT0 = 0x00000001
	BIT1 = 0x00000002
	BIT2 = 0x00000004
	BIT3 = 0x00000008
	BIT4 = 0x00000010
	BIT5 = 0x00000020
	BIT6 = 0x00000040
	BIT7 = 0x00000080
	BIT8 = 0x00000100
	BIT9 = 0x00000200
	BIT10 = 0x00000400
	BIT11 = 0x00000800
	BIT12 = 0x00001000
	BIT13 = 0x00002000
	BIT14 = 0x00004000
	BIT15 = 0x00008000
	BIT16 = 0x00010000

	_openFile = None

	# '>' big endian
	# '<' little endian
	endianness = '<'

	customSymbols = None

	_uncompressBuffer = None
	_bufferPointer = 0

	def __init__(self, openFile):
		self._openFile = openFile

	#NOTE: clears the uncompress buffer
	def seek(self, byteOffset):
		self._uncompressBuffer = None
		self._bufferPointer = 0
		self._openFile.seek(byteOffset)


	def calcSize(self, binFormat):
		strucFormat = self.endianness + binFormat
		return struct.calcsize(strucFormat)

	def uncompressSegment(self, compressLength, compressionAlgorithm):
		compressData = self._openFile.read(compressLength)
		if compressionAlgorithm == ZLIB_COMPRESSION:
			# uncompress Logical Element Header
			self._uncompressBuffer = zlib.decompress(compressData)
			self._bufferPointer = 0

	def rawInputRead(self, numOfBytes):
		if self._uncompressBuffer != None and self._bufferPointer < len(self._uncompressBuffer):
			if self._bufferPointer + numOfBytes >= len(self._uncompressBuffer):
				raise Exception("reading passed uncompressed buffer area")
			rawData = self._uncompressBuffer[self._bufferPointer : self._bufferPointer + numOfBytes]
			self._bufferPointer += numOfBytes
			return rawData
		return self._openFile.read(numOfBytes)

	def readStruct(self, binFormat):

		#look for custom symbols
		for key in self.customSymbols:
			pos = binFormat.find(key)
			if pos != -1:
				#hit custom symbol

				#process before
				before = self.readStruct(binFormat[0:pos])
				#process custom symbol
				special = self.customSymbols[key](self)
				#process after
				after = self.readStruct(binFormat[pos+1:])

				returnValue = before + special + after
				return returnValue



		strucFormat = self.endianness + binFormat
		bufferLen = struct.calcsize(strucFormat)
		buffer = self.rawInputRead(bufferLen)
		return struct.unpack(strucFormat, buffer)

	def _readGUID(self):
		binFormat = "IHHBBBBBBBB"
		strucFormat = self.endianness + binFormat
		bufferLen = struct.calcsize(strucFormat)
		buffer = self.rawInputRead(bufferLen)
		guid = struct.unpack(strucFormat, buffer)

		# wrap return into a tuple
		guidInTuple = ( guid, )
		return guidInTuple

	def _readMString(self):
		(charCount, ) = self.readStruct("i")
		if charCount < 0:
			raise Exception("failed to read MString. Length is negative number")

		byteCount = charCount * 2
		(encodedString, ) = self.readStruct(str(byteCount) + "s")
		messageStr = encodedString.decode("utf-16")
		#wrap return into a tuple
		messageInTuple = (messageStr, )
		return messageInTuple

	def _readCoordF32(self):
		xyzValues = self.readStruct("3f")
		xyzTuple = (xyzValues, )
		return xyzTuple

	def _readBBoxF32(self):
		minMaxCorners = self.readStruct("oo")
		minMaxInTuple = (minMaxCorners, )
		return minMaxInTuple

	def _readVecI32(self):
		(dataCount, ) = self.readStruct("i")
		data = self.readStruct(str(dataCount) + "i")
		# wrap return into a tuple
		dataInTuple = (data,)
		return dataInTuple





	customSymbols = {
						'G' : _readGUID,
					 	'M' : _readMString,
						'o' : _readCoordF32,
						'X' : _readBBoxF32,
						'V' : _readVecI32,
					 }
